import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent, AComponent, BComponent } from './app.component';

@NgModule({
	declarations: [AppComponent, AComponent, BComponent],
	imports: [BrowserModule],
	providers: [],
	entryComponents: [AppComponent, AComponent, BComponent]
})
export class AppModule {

	ngDoBootstrap(app) {
		this.fetch('url/to/fetch/component/name')
			.then((name) => { this.bootstrapRootComponent(app, name) });
	}
	
	fetch(url) {
		return new Promise((resolve) => {
			setTimeout(() => {
				resolve('app-root');
			}, 2000);
		});
	}

	bootstrapRootComponent(app, name) {
		const options = {
		  'a-comp': AComponent,
		  'b-comp': BComponent,
		  'app-root': AppComponent
		};
		
		const statusElement = document.querySelector('#status');
		statusElement.textContent = '';
		
		const componentElement = document.createElement(name);
		document.body.appendChild(componentElement);
		
		const component = options[name];
		app.bootstrap(component);
	  }
}
